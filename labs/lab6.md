*Version1:*

Scrooge McDuck's grandnephews, Huey, Dewey, and Louie, are working on two Git projects. One project is their "Money_Bin" project, a financial management system hosted on GitLab, and the other is a secret project known only to Dewey. The grandnephews have different work setups and collaborate in various ways. Describe their Git project collaboration scenario schematically. 

Each git repo should be marked as a rectangle. Every git repo should be signed with an owner name, and repo name default in git terminology if not specified otherwise, and if necessary additional information. Relationships/actions (who clones or forks where and from, who pushes, pulls or fetches from where) between those repos should be drowned by arrows, and arrows signed by a relationship/action.

The “Money_Bin” project is hosted on GitLab. Dewey sometimes works from the PC station at his Uncle Scrooge house, but sometimes he works from a library PC in school, where each student has some individual disk space. Huey shares the same PC with Dewey in their Uncle Scrooge house, and doesn’t do any work from other places. Louie has his own laptop which he bought from his Uncle Scrooge by saving his pocket money. Often Louie and Huey work together before sharing their common work with Dewey. Dewey works on his super secret project only from a library PC.

*Version2:*

Mickey Mouse, Bugs Bunny and Snow White three iconic cartoon characters, have a shared passion for technology and programming. They are collaborating on various Git projects, some of which are common while others are individual. Describe their Git project collaboration scenario schematically.

Each git repo should be marked as a cycle, square or rectangle. Every git repo should be signed with an owner name, and repo name default in git terminology if not specified otherwise, and if necessary any additional information. Relationships/actions (who clones or forks where and from, who pushes, pulls or fetches from where) between those repos should be drowned by arrows, and arrows signed according to those relationship/actions.

The “DisneyPhoneBook” developed by Mickey and Bunny is hosted on GitLab. Mickey works from his PC station, sometimes from his girlfriend's Minnie Mouse laptop, and sometimes he uses his best friend Guffy's phone to call Bunny.  Bunny uses his laptop only. Mickey has his own “DisneyMaps” git project and he is the only contributor there. Snow White with the help of Bunny created the “DisneyTinder” project. Snow White works from her Apple laptop.
