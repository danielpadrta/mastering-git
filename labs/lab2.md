# Lab 2

* git branch, --list, -v, -vv, -a

* git switch

* git switch -c, -C, git restore [--staged], git checkout

* .git/refs .git/HEAD

* git commit -a -m -v

* git branch -m

* git merge, --ff, --no-ff, --ff-only

* git branch -d

* git push [origin [:remote_branch_to_delete]]
